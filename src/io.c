
#include "io.h"

void console_print(char* message){
  fprintf(stdout, "%s", message);
}

void log_print(char* message){
  fprintf(file_ptr, "%s", message);
}

void debug_log(char* message){
  if (!file_ptr){
   file_ptr = fopen("dump.log","w"); 
  }
  
  char * alt_message = (char*)malloc (sizeof(char)*max_length);
  memset(alt_message, '\0', max_length);
  sprintf(alt_message, "%s\n", message);
  
  console_print(alt_message);
  log_print(alt_message);

  free(alt_message);
}

void clean_up_io(){
  debug_log("Closing FD of debugger log");
  if (file_ptr){
    fclose(file_ptr);
  }
}
