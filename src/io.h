
#ifndef IO_H
#define IO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern FILE* file_ptr;

#define max_length 256
#define IN_OUT_ERROR 2

//Prints to console
void console_print(char* message);
//Prints to log
void log_print(char* message);
//Call log and console printing
void debug_log(char* message);
//Close all opened file descriptors
void clean_up_io();

#endif //IO_H
