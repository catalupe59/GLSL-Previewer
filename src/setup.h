
#ifndef SETUP_H
#define SETUP_H

#include <stdlib.h>

//#include "../include/GL/glew.h"
#include "../include/GLFW/glfw3.h"

#include "io.h"

#define GLFW_ERROR -2

void init ();
void clean_up();

#endif // SETUP_H
