
#include "setup.h"

void init(){
  if (!glfwInit()){
    debug_log("Failed to init glfw, exiting");
    exit(GLFW_ERROR);
  }

  glfwWindowHint(GLFW_SAMPLES,4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  
}

void clean_up(){

}
